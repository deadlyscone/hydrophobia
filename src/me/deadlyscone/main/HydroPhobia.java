package me.deadlyscone.main;

import me.deadlyscone.events.onSwimEvent;

import org.bukkit.plugin.java.JavaPlugin;

public class HydroPhobia extends JavaPlugin{
	
	@Override
	public void onEnable() {
		registerEvents();
	}
	
	@Override
	public void onDisable() {
		
	}
	
	private void registerEvents(){
		new onSwimEvent(this);
	}
}
