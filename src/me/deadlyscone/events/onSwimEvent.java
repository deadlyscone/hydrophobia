package me.deadlyscone.events;

import me.deadlyscone.data.DataManager;
import me.deadlyscone.main.HydroPhobia;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class onSwimEvent implements Listener{
	
	public onSwimEvent(HydroPhobia plugin){
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerMoveToWater(PlayerMoveEvent e){
		Player player = e.getPlayer();
		Material m = e.getPlayer().getLocation().getBlock().getType();
		Material mL = e.getPlayer().getLocation().subtract(0,1,0).getBlock().getType();
		
		/*
		 * Store the location if the player is on the ground
		 */
		if (player.isOnGround() && m != Material.STATIONARY_WATER && m != Material.WATER && mL != Material.AIR) {
    		DataManager.putPlayerLastLocation(player);
    		
    		/*
    		 * Player contacted water
    		 */
    	}else if (!player.isOnGround() && (m == Material.STATIONARY_WATER || m == Material.WATER)) {
    		Location mem = DataManager.getPlayerLastLocation(player.getUniqueId());
    		
    		
	        if(mem != null){
	        	Location mLoc = new Location( mem.getWorld(), mem.getX(), mem.getY(), mem.getZ(), mem.getYaw(), mem.getPitch());
	        	Location b = new Location(mLoc.getWorld(), mLoc.getX(), mLoc.getY(), mLoc.getZ(), mLoc.getYaw(), mLoc.getPitch());
	        	Material mLocBlock = b.subtract(0,1,0).getBlock().getType();
	        	Boolean wasSwimming = false;
	        	/*
	        	 * Additional check: Checks to see if the memory location block is water, 
	        	 * if so then teleports the player 0.5 block back in the oppisite
	        	 * direction, avoiding an infinite fall.
	        	 */
	        	if(mLocBlock == Material.STATIONARY_WATER || mLocBlock == Material.STATIONARY_WATER || mLocBlock == Material.AIR || 
	        			mLocBlock == Material.FENCE || mLocBlock == Material.IRON_FENCE || mLocBlock == Material.BIRCH_FENCE){
	        		Location f = new Location(mLoc.getWorld(), mLoc.getX(), mLoc.getY(), mLoc.getZ(), mLoc.getYaw(), mLoc.getPitch());
	        		Location dir = getDirection(mLoc, player);
	        		Location tele = new Location(player.getWorld(), (f.getX()+dir.getX()), (f.getY()+dir.getY()), (f.getZ()+dir.getZ()), f.getYaw(), f.getPitch());
	        		player.setFallDistance(0f);
	        		player.teleport(tele);
	        		wasSwimming = true;
	        		
	        	}else{
	        		player.setFallDistance(0f);
	        		player.teleport(mLoc);
	        		wasSwimming = true;
	        	}
	        	
	        	/*
	        	 * Send the player our Message
	        	 */
	        	if(wasSwimming){
	        		player.sendMessage(ChatColor.RED + "You are not allowed to swim.");
	        	}
	        }
	    }
	}
	
	public Location getCenter(Location loc) {
	    return new Location(loc.getWorld(),
	        getRelativeCoord(loc.getBlockX()),
	        getRelativeCoord(loc.getBlockY()),
	        getRelativeCoord(loc.getBlockZ()),
	        loc.getYaw(),
	        loc.getPitch()
	        );
	}
	 
	private double getRelativeCoord(int i) {
	    double d = i;
	    d = d < 0 ? d - .5 : d + .5;
	    return d;
	}
	
	private Location getDirection(Location l, Player player){
		Location pLoc = new Location(player.getWorld(), player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ(), player.getLocation().getYaw(), player.getLocation().getPitch());
		Location in = l.subtract(pLoc);
		
		Location u = new Location(
		player.getWorld(), 
		(in.getX() / Math.abs(in.getX())), 
		(in.getY() / Math.abs(in.getY())), 
		(in.getZ() / Math.abs(in.getZ()))
		);
		
		if(Double.isNaN(u.getX())){
			u = new Location(u.getWorld(), 0.0, u.getY(), u.getZ());
		}
		if(Double.isNaN(u.getY())){
			u = new Location(u.getWorld(), u.getX(), 0.0, u.getZ());
		}
		if(Double.isNaN(u.getZ())){
			u = new Location(u.getWorld(), u.getX(), u.getY(), 0.0);
		}
		u.multiply(0.5);
	return u;
	}
	

}