package me.deadlyscone.data;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class DataManager {
	
	public static HashMap<UUID, Location> playerLastLocation= new HashMap<UUID, Location>();
	
	public static Location getPlayerLastLocation(UUID playerUUID){
		if(playerLastLocation.containsKey(playerUUID)){
			return playerLastLocation.get(playerUUID);
		}
		return null;
	}
	
	public static void putPlayerLastLocation(Player player){
		playerLastLocation.put(player.getUniqueId(), player.getLocation());
		
	}
	
	public static void deletePlayerLastLocation(UUID playerUUID){
			playerLastLocation.remove(playerUUID);
	}

}